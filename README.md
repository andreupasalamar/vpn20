# vpn20

VPN EDT

1. Tunel sense encriptar

	`openvpn --remote <host desti> --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --verb 2`

2. Secret compartit

	1. `openvpn --gen-key --secret clau`
	2. Copiar la clau
	3. `openvpn --remote <host desti> --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --secret clau --verb 2`

3. Clau publica , clau privada

	`openvpn --remote <host desti> --dev tun1 --ifconfig 10.4.0.1 10.4.0.2 --tls-client [--tls-server] --ca ca.crt --cert client.crt [server.crt] --key client.key [server.key] --reneg-sec 60 [--dh dh1024.pem] --verb 2`


